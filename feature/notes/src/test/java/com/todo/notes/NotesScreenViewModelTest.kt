package com.todo.notes

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.todo.domain.app_settings.GetAppSettingsUseCase
import com.todo.domain.app_settings.UpdateNoteSortTypeUseCase
import com.todo.domain.notes.GetAllNoteUseCase
import com.todo.domain.notes.SearchNoteUseCase
import com.todo.model.AppSettings
import com.todo.model.NoteColor
import com.todo.model.NoteResource
import com.todo.model.SortViewButtonStates
import com.todo.model.createNoteResource
import com.todo.notes.states.NotesScreenEvents
import com.todo.notes.states.NotesScreenState
import com.todo.testing.utils.MainDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.anyString
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
@MediumTest
class NotesScreenViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    lateinit var getAllNoteUseCase: GetAllNoteUseCase

    @Mock
    lateinit var searchNoteUseCase: SearchNoteUseCase

    @Mock
    lateinit var getAppSettingsUseCase: GetAppSettingsUseCase

    @Mock
    lateinit var updateNoteSortTypeUseCase: UpdateNoteSortTypeUseCase

    private lateinit var viewModel: NotesScreenViewModel

    @Before
    fun setup() {
        createViewModel()
    }

    private fun createViewModel() {
        viewModel = NotesScreenViewModel(
            getAllNoteUseCase,
            searchNoteUseCase,
            getAppSettingsUseCase,
            updateNoteSortTypeUseCase
        )
    }

    @Test
    fun `test view model ui state at first initialization`() {
        assertThat(viewModel.notesScreenUiState.value is NotesScreenState.Loading).isTrue()
    }

    @Test
    fun `test view model ui state when notes fetched from database`() {
        `when`(getAllNoteUseCase.invoke()).thenReturn(flow { emit(createListOfNotes()) })
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        runTest {
            verify(getAllNoteUseCase).invoke()
        }
        assertThat(viewModel.notesScreenUiState.value is NotesScreenState.Success).isTrue()
    }

    private fun createListOfNotes() = mutableListOf<NoteResource>().apply {
        for (i in 1..10)
            add(
                createNoteResource(
                    noteTitle = "Title number $i",
                    noteDescription = "Description number $i",
                    noteColor = NoteColor.RED
                )
            )
    }

    @Test
    fun `test view model ui state when empty note list fetched from database`() {
        `when`(getAllNoteUseCase.invoke()).thenReturn(flow { emit(emptyList()) })
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        runTest {
            verify(getAllNoteUseCase).invoke()
        }
        assertThat(viewModel.notesScreenUiState.value is NotesScreenState.EmptyList).isTrue()
    }

    @Test
    fun `test view model ui state when user search in there notes`() {
        `when`(searchNoteUseCase.invoke(anyString())).thenReturn(flow { emit(createListOfNotes()) })
        `when`(getAllNoteUseCase.invoke()).thenReturn(flow { emit(emptyList()) })
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        viewModel.onEvent(NotesScreenEvents.Search("query"))
        runTest {
            verify(searchNoteUseCase).invoke(anyString())
        }
        assertThat(viewModel.notesScreenUiState.value is NotesScreenState.Success).isTrue()
    }

    @Test
    fun `test view model ui state when user search in there notes but no result found`() {
        `when`(searchNoteUseCase.invoke(anyString())).thenReturn(flow { emit(emptyList()) })
        `when`(getAllNoteUseCase.invoke()).thenReturn(flow { emit(emptyList()) })
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        viewModel.onEvent(NotesScreenEvents.Search("query"))
        runTest {
            verify(searchNoteUseCase).invoke(anyString())
        }
        assertThat(viewModel.notesScreenUiState.value is NotesScreenState.SearchResultNotFount).isTrue()
    }

    @Test
    fun `test view model ui state when user change note sort methode`() {
        `when`(getAllNoteUseCase.invoke()).thenReturn(flow { emit(emptyList()) })
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        viewModel.onEvent(NotesScreenEvents.ChangeNoteSortType(SortViewButtonStates.GRID))
        runTest {
            verify(updateNoteSortTypeUseCase).invoke(SortViewButtonStates.GRID)
        }
    }
}