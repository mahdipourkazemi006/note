package com.todo.notes.states

import com.todo.model.SortViewButtonStates

sealed class NotesScreenEvents {

    data class Search(val query : String) : NotesScreenEvents()

    data class ChangeNoteSortType(val sortType : SortViewButtonStates) : NotesScreenEvents()

}
