package com.todo.notes.states

import com.todo.model.NoteResource

sealed interface NotesScreenState {
    data object EmptyList : NotesScreenState

    data object SearchResultNotFount : NotesScreenState

    data object Loading : NotesScreenState

    data class Success(
        val notes : List<NoteResource>
    ) : NotesScreenState
}
