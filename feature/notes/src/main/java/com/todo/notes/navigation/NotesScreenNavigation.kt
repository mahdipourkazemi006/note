package com.todo.notes.navigation

import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.todo.model.NoteResource
import com.todo.notes.NotesScreen

const val notesScreenRoute = "notes_screen_route"

fun NavGraphBuilder.notesScreen(
    onAddNewNoteClick: () -> Unit,
    onSettingButtonClick: () -> Unit,
    onNoteClick: (NoteResource) -> Unit
) {
    composable(route = notesScreenRoute) {
        NotesScreen(
            onAddNewNoteClick = onAddNewNoteClick,
            onNoteClick = onNoteClick,
            onSettingButtonClick = onSettingButtonClick
        )
    }
}
