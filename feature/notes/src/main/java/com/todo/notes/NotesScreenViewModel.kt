package com.todo.notes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.todo.common.asResult
import com.todo.domain.app_settings.GetAppSettingsUseCase
import com.todo.domain.app_settings.UpdateNoteSortTypeUseCase
import com.todo.domain.notes.GetAllNoteUseCase
import com.todo.domain.notes.SearchNoteUseCase
import com.todo.model.AppSettings
import com.todo.notes.states.NotesScreenEvents
import com.todo.notes.states.NotesScreenState
import com.todo.notes.utils.mapToSearchUiState
import com.todo.notes.utils.mapToUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotesScreenViewModel @Inject constructor(
    private val getAllNoteUseCase: GetAllNoteUseCase,
    private val searchNoteUseCase: SearchNoteUseCase,
    private val getAppSettingsUseCase: GetAppSettingsUseCase,
    private val updateNoteSortTypeUseCase: UpdateNoteSortTypeUseCase
) : ViewModel() {

    private var _notesScreenUiState = MutableStateFlow<NotesScreenState>(NotesScreenState.Loading)
    val notesScreenUiState: StateFlow<NotesScreenState> get() = _notesScreenUiState

    private var _appSettings = MutableStateFlow(AppSettings())
    val appSettings : StateFlow<AppSettings> get() = _appSettings

    init {
        viewModelScope.launch {
            getAppSettingsUseCase.invoke().collect {
                _appSettings.value = it
            }
        }
        viewModelScope.launch {
            getAllNoteUseCase.invoke().asResult().mapToUiState().collect {
                _notesScreenUiState.value = it
            }
        }
    }

    fun onEvent(event : NotesScreenEvents) = viewModelScope.launch {
        when(event) {
            is NotesScreenEvents.Search ->
                searchNoteUseCase.invoke(event.query).asResult().mapToSearchUiState().collect {
                    _notesScreenUiState.value = it
                }
            is NotesScreenEvents.ChangeNoteSortType ->
                updateNoteSortTypeUseCase.invoke(event.sortType)
        }
    }
}