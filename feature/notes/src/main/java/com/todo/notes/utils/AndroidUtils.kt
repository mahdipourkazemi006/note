package com.todo.notes.utils

import com.todo.common.Result
import com.todo.model.NoteResource
import com.todo.notes.states.NotesScreenState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

fun Flow<Result<List<NoteResource>>>.mapToUiState() = map {
    when (it) {
        is Result.Success -> if (it.data.isEmpty()) NotesScreenState.EmptyList
        else NotesScreenState.Success(notes = it.data)

        is Result.Loading -> NotesScreenState.Loading
        is Result.Error -> NotesScreenState.EmptyList
    }
}

fun Flow<Result<List<NoteResource>>>.mapToSearchUiState() = map {
    when (it) {
        is Result.Success -> if (it.data.isEmpty()) NotesScreenState.SearchResultNotFount
        else NotesScreenState.Success(notes = it.data)

        is Result.Loading -> NotesScreenState.Loading
        is Result.Error -> NotesScreenState.EmptyList
    }
}