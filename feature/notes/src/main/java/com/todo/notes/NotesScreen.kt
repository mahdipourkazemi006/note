package com.todo.notes

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.todo.designsystem.component.SortViewButton
import com.todo.designsystem.component.TextBodyMedium
import com.todo.designsystem.component.TextHeaderLarge
import com.todo.designsystem.component.TextTitleSmall
import com.todo.designsystem.icon.AppOutlinedIcons
import com.todo.designsystem.icon.AppRoundedIcons
import com.todo.model.NoteResource
import com.todo.model.SortViewButtonStates
import com.todo.notes.states.NotesScreenEvents
import com.todo.notes.states.NotesScreenState
import com.todo.ui.NoteItem
import com.todo.ui.SearchBox
import com.todo.ui.ToVerticalSpace
import com.todo.ui.isScrollingUp

@Composable
fun NotesScreen(
    onAddNewNoteClick: () -> Unit,
    onSettingButtonClick: () -> Unit,
    onNoteClick: (NoteResource) -> Unit,
    viewModel: NotesScreenViewModel = hiltViewModel()
) {
    val lazyListState = rememberLazyListState()
    val lazyGridState = rememberLazyGridState()
    val appSettings by viewModel.appSettings.collectAsStateWithLifecycle()
    val uiState by viewModel.notesScreenUiState.collectAsStateWithLifecycle()
    val searchBoxField = remember {
        mutableStateOf("")
    }
    Scaffold(
        topBar = {
            TopBar(
                sortViewButtonStates = appSettings.noteSort,
                searchBoxValue = searchBoxField.value,
                onSearched = {
                    searchBoxField.value = it
                    viewModel.onEvent(NotesScreenEvents.Search(it))
                },
                isNotesListEmpty = uiState is NotesScreenState.EmptyList,
                onSettingButtonClick = onSettingButtonClick,
                showNoteSortButton = uiState is NotesScreenState.Success,
                onSortViewChanged = {
                    viewModel.onEvent(
                        NotesScreenEvents.ChangeNoteSortType(
                            it
                        )
                    )
                }
            )
        },
        floatingActionButton = {
            AnimatedVisibility(
                visible = when (appSettings.noteSort) {
                    SortViewButtonStates.GRID -> lazyGridState.isScrollingUp()
                    SortViewButtonStates.LIST -> lazyListState.isScrollingUp()
                },
                enter = scaleIn(),
                exit = scaleOut()
            ) {
                FloatingActionButton(
                    containerColor = MaterialTheme.colorScheme.primary,
                    onClick = { onAddNewNoteClick() }) {
                    Icon(
                        imageVector = AppRoundedIcons.Add,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        }
    ) { padding ->
        when (uiState) {
            is NotesScreenState.SearchResultNotFount -> {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(padding),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    26.ToVerticalSpace()
                    TextTitleSmall(
                        text = "No result found for \"${searchBoxField.value}\" !",
                        color = MaterialTheme.colorScheme.onBackground
                    )
                }
            }

            is NotesScreenState.Loading -> {}
            is NotesScreenState.Success -> {
                val notes = (uiState as NotesScreenState.Success).notes
                when (appSettings.noteSort) {
                    SortViewButtonStates.LIST -> {
                        LazyColumn(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(
                                    top = padding.calculateTopPadding(),
                                    start = 16.dp,
                                    end = 16.dp
                                ),
                            state = lazyListState
                        ) {
                            items(notes.size) {
                                NoteItem(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(end = 6.dp),
                                    note = notes[it],
                                    appTheme = appSettings.theme,
                                    onNoteClick = onNoteClick
                                )
                                16.ToVerticalSpace()
                            }
                        }
                    }

                    SortViewButtonStates.GRID -> {
                        LazyVerticalGrid(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(
                                    top = padding.calculateTopPadding(),
                                    start = 16.dp,
                                    end = 16.dp
                                ),
                            state = lazyGridState,
                            columns = GridCells.Fixed(2)
                        ) {
                            items(notes.size) {
                                NoteItem(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(end = 6.dp),
                                    note = notes[it],
                                    appTheme = appSettings.theme,
                                    onNoteClick = onNoteClick
                                )
                                16.ToVerticalSpace()
                            }
                        }
                    }
                }
            }

            is NotesScreenState.EmptyList -> {
                EmptyNotesAnimation()
            }
        }
    }
}

@Composable
private fun TopBar(
    sortViewButtonStates: SortViewButtonStates,
    searchBoxValue: String,
    isNotesListEmpty: Boolean,
    showNoteSortButton: Boolean,
    onSearched: (String) -> Unit,
    onSettingButtonClick: () -> Unit,
    onSortViewChanged: (SortViewButtonStates) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalAlignment = Alignment.Start
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextHeaderLarge(text = "Notes", color = MaterialTheme.colorScheme.onBackground)
            IconButton(onClick = onSettingButtonClick) {
                Icon(
                    modifier = Modifier.size(28.dp),
                    imageVector = AppOutlinedIcons.Settings,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onBackground
                )
            }
        }
        if (!isNotesListEmpty) {
            12.ToVerticalSpace()
            SearchBox(
                Modifier
                    .fillMaxWidth()
                    .padding(end = 8.dp),
                placeholder = stringResource(R.string.find_your_notes),
                value = searchBoxValue,
                onSearch = onSearched
            )
            if (showNoteSortButton) {
                28.ToVerticalSpace()
                SortViewButton(
                    modifier = Modifier.size(40.dp),
                    buttonState = sortViewButtonStates,
                    onClick = onSortViewChanged
                )
            }
        }
    }
}

@Composable
private fun EmptyNotesAnimation() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.lottie_empty_notes_animation))
    val progress by animateLottieCompositionAsState(composition)
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        LottieAnimation(
            modifier = Modifier.size(360.dp),
            composition = composition,
            contentScale = ContentScale.Crop,
            progress = { progress },
        )
        TextBodyMedium(
            text = stringResource(R.string.no_notes_yet_your_canvas_awaits),
            textAlign = TextAlign.Center,
            color = MaterialTheme.colorScheme.onBackground
        )
    }
}