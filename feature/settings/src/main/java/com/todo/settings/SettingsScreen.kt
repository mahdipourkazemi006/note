package com.todo.settings

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonColors
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.todo.designsystem.component.TextLabelMedium
import com.todo.designsystem.component.TextTitleMedium
import com.todo.model.AppTheme
import com.todo.settings.states.SettingsScreenUiEvents
import com.todo.settings.states.SettingsScreenUiState
import com.todo.ui.ToVerticalSpace

@Composable
fun SettingsScreen(
    viewModel: SettingsScreenViewModel = hiltViewModel()
) {
    val uiState = viewModel.settingsScreenUiState.collectAsStateWithLifecycle().value
    val appTheme = if (uiState is SettingsScreenUiState.Success) uiState.appSettings.theme
    else AppTheme.LIGHT
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                color = MaterialTheme.colorScheme.background,
                shape = Shapes().medium
            )
            .padding(16.dp),
        horizontalAlignment = Alignment.Start
    ) {
        TextTitleMedium(
            text = stringResource(R.string.theme),
            color = MaterialTheme.colorScheme.onBackground
        )
        16.ToVerticalSpace()
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            RadioButton(
                selected = appTheme == AppTheme.DARK,
                colors = RadioButtonColors(
                    selectedColor = MaterialTheme.colorScheme.onBackground,
                    unselectedColor = MaterialTheme.colorScheme.onBackground,
                    disabledSelectedColor = MaterialTheme.colorScheme.onBackground,
                    disabledUnselectedColor = MaterialTheme.colorScheme.onBackground
                ),
                onClick = {
                    viewModel.onEvent(
                        SettingsScreenUiEvents.UpdateAppTheme(AppTheme.DARK)
                    )
                })
            TextLabelMedium(text = "Dark", color = MaterialTheme.colorScheme.onBackground)
        }
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            RadioButton(
                selected = appTheme == AppTheme.LIGHT,
                colors = RadioButtonColors(
                    selectedColor = MaterialTheme.colorScheme.onBackground,
                    unselectedColor = MaterialTheme.colorScheme.onBackground,
                    disabledSelectedColor = MaterialTheme.colorScheme.onBackground,
                    disabledUnselectedColor = MaterialTheme.colorScheme.onBackground
                ),
                onClick = {
                    viewModel.onEvent(
                        SettingsScreenUiEvents.UpdateAppTheme(AppTheme.LIGHT)
                    )
                })
            TextLabelMedium(text = "Light", color = MaterialTheme.colorScheme.onBackground)
        }
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            RadioButton(
                selected = appTheme == AppTheme.AUTOMATIC,
                colors = RadioButtonColors(
                    selectedColor = MaterialTheme.colorScheme.onBackground,
                    unselectedColor = MaterialTheme.colorScheme.onBackground,
                    disabledSelectedColor = MaterialTheme.colorScheme.onBackground,
                    disabledUnselectedColor = MaterialTheme.colorScheme.onBackground
                ),
                onClick = {
                    viewModel.onEvent(
                        SettingsScreenUiEvents.UpdateAppTheme(AppTheme.AUTOMATIC)
                    )
                })
            TextLabelMedium(text = "Automatic", color = MaterialTheme.colorScheme.onBackground)
        }
    }
}