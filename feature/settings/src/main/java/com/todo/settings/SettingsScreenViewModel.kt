package com.todo.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.todo.domain.app_settings.GetAppSettingsUseCase
import com.todo.domain.app_settings.UpdateAppThemeUseCase
import com.todo.settings.states.SettingsScreenUiEvents
import com.todo.settings.states.SettingsScreenUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsScreenViewModel @Inject constructor(
    private val getAppSettingsUseCase: GetAppSettingsUseCase,
    private val updateAppThemeUseCase: UpdateAppThemeUseCase
) : ViewModel() {

    private var _settingsScreenUiState = MutableStateFlow<SettingsScreenUiState>(SettingsScreenUiState.Nothing)
    val settingsScreenUiState : StateFlow<SettingsScreenUiState> get() = _settingsScreenUiState

    init {
        viewModelScope.launch {
            getAppSettingsUseCase.invoke().collect {
                _settingsScreenUiState.value = SettingsScreenUiState.Success(it)
            }
        }
    }

    fun onEvent(event : SettingsScreenUiEvents) = viewModelScope.launch {
        when(event) {
            is SettingsScreenUiEvents.UpdateAppTheme -> {
                updateAppThemeUseCase.invoke(event.theme)
            }
        }
    }

}