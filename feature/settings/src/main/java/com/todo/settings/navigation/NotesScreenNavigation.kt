package com.todo.settings.navigation

import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.dialog
import com.todo.settings.SettingsScreen

const val settingsScreenRoute = "settings_screen_route"

fun NavController.navigateToSettingsScreen(navOptions: NavOptions? = null) {
    this.navigate(settingsScreenRoute, navOptions)
}

fun NavGraphBuilder.settingsScreen() {
    dialog(
        route = settingsScreenRoute,
        dialogProperties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = true,
        )
    ) {
        SettingsScreen()
    }
}
