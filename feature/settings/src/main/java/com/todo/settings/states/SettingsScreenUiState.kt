package com.todo.settings.states

import com.todo.model.AppSettings

sealed interface SettingsScreenUiState {

    data class Success(val appSettings: AppSettings) : SettingsScreenUiState
    data object Nothing : SettingsScreenUiState

}