package com.todo.settings.states

import com.todo.model.AppTheme

sealed interface SettingsScreenUiEvents {

    data class UpdateAppTheme(val theme : AppTheme) : SettingsScreenUiEvents

}