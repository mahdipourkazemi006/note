package com.todo.settings

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.todo.domain.app_settings.GetAppSettingsUseCase
import com.todo.domain.app_settings.UpdateAppThemeUseCase
import com.todo.model.AppSettings
import com.todo.model.AppTheme
import com.todo.settings.states.SettingsScreenUiEvents
import com.todo.settings.states.SettingsScreenUiState
import com.todo.testing.utils.MainDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
@MediumTest
class SettingsScreenViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    lateinit var getAppSettingsUseCase: GetAppSettingsUseCase

    @Mock
    lateinit var updateAppThemeUseCase: UpdateAppThemeUseCase

    private lateinit var viewModel: SettingsScreenViewModel

    @Before
    fun setup() {
        viewModel = SettingsScreenViewModel(getAppSettingsUseCase, updateAppThemeUseCase)
    }

    @Test
    fun `test view model ui state first initialization`() {
        assertThat(viewModel.settingsScreenUiState.value is SettingsScreenUiState.Nothing).isTrue()
    }

    @Test
    fun `test view model ui state when app setting fetched from preferences`() {
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        runTest {
            verify(getAppSettingsUseCase).invoke()
            assertThat(viewModel.settingsScreenUiState.value is SettingsScreenUiState.Success).isTrue()
        }
    }

    @Test
    fun `test view model change theme`() {
        `when`(getAppSettingsUseCase.invoke()).thenReturn(flow { emit(AppSettings()) })
        viewModel.onEvent(SettingsScreenUiEvents.UpdateAppTheme(AppTheme.DARK))
        runTest {
            verify(updateAppThemeUseCase).invoke(AppTheme.DARK)
        }
    }
}