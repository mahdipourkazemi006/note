package com.todo.newnote.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.todo.newnote.NewNoteScreen

const val NOTE_ARG = "note"
const val newNoteScreenRoute = "new_note_screen_route"
const val newNoteScreenRouteWithArg = "new_note_screen_route/{note}"

fun NavController.navigateToNewNoteScreen(navOptions: NavOptions? = null) {
    this.navigate(newNoteScreenRoute, navOptions)
}

fun NavController.navigateToEditNoteScreen(noteId: Int, navOptions: NavOptions? = null) {
    this.navigate("new_note_screen_route/$noteId", navOptions)
}

fun NavGraphBuilder.newNoteScreen(
    onBackClick: () -> Unit
) {
    composable(
        route = newNoteScreenRouteWithArg,
        arguments = listOf(
            navArgument(NOTE_ARG) {
                type = NavType.IntType
            }
        )
    ) {
        NewNoteScreen(onBackClick = onBackClick)
    }
    composable(
        route = newNoteScreenRoute
    ) {
        NewNoteScreen(onBackClick = onBackClick)
    }
}
