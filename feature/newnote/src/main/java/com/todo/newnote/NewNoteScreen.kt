package com.todo.newnote

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.todo.designsystem.component.BodyTextField
import com.todo.designsystem.component.TextBodyLarge
import com.todo.designsystem.component.TextBodyMedium
import com.todo.designsystem.component.TitleTextField
import com.todo.designsystem.icon.AppOutlinedIcons
import com.todo.designsystem.icon.AppRoundedIcons
import com.todo.designsystem.theme.DarkNoteGreenBackground
import com.todo.designsystem.theme.DarkNoteRedBackground
import com.todo.designsystem.theme.DarkNoteYellowBackground
import com.todo.model.NoteColor
import com.todo.newnote.event.NewNoteScreenEvents
import com.todo.newnote.state.NewNoteScreenUiState
import com.todo.ui.ToHorizontalSpace
import com.todo.ui.ToVerticalSpace
import com.todo.ui.dateTimeFormat

private const val SELECTED_NOTE_COLOR_ALPHA = 0.3f

@Composable
fun NewNoteScreen(
    onBackClick: () -> Unit,
    vieWModel: NewNoteScreenViewModel = hiltViewModel()
) {
    val uiState by vieWModel.newNoteScreenUiState.collectAsStateWithLifecycle()
    val dateTime = remember {
        mutableLongStateOf(
            if (uiState is NewNoteScreenUiState.EditNote)
                (uiState as NewNoteScreenUiState.EditNote).note?.creationDate ?: 0L
            else System.currentTimeMillis()
        )
    }
    val noteTitleValue = remember {
        mutableStateOf(
            if (uiState is NewNoteScreenUiState.EditNote)
                (uiState as NewNoteScreenUiState.EditNote).note?.title ?: ""
            else ""
        )
    }
    val noteDescriptionValue = remember {
        mutableStateOf(
            if (uiState is NewNoteScreenUiState.EditNote)
                (uiState as NewNoteScreenUiState.EditNote).note?.description ?: ""
            else ""
        )
    }
    val selectedNoteColor = remember {
        mutableStateOf(
            if (uiState is NewNoteScreenUiState.EditNote)
                (uiState as NewNoteScreenUiState.EditNote).note?.color ?: NoteColor.GREEN
            else
                NoteColor.GREEN
        )
    }
    BackHandler(enabled = true) {
        vieWModel.onEvent(
            NewNoteScreenEvents.AddOrUpdateNote(
                noteTitleValue.value,
                noteDescriptionValue.value,
                selectedNoteColor.value
            )
        )
        onBackClick()
    }
    Box(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp, end = 16.dp, start = 16.dp, bottom = 38.dp)
                .align(Alignment.TopCenter),
            horizontalAlignment = Alignment.Start
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Row(
                    modifier = Modifier.clickable {
                        onBackClick()
                        vieWModel.onEvent(
                            NewNoteScreenEvents.AddOrUpdateNote(
                                noteTitleValue.value,
                                noteDescriptionValue.value,
                                selectedNoteColor.value
                            )
                        )
                    }
                ) {
                    Icon(
                        modifier = Modifier.size(16.dp),
                        imageVector = AppRoundedIcons.Back,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.secondary
                    )
                    6.ToHorizontalSpace()
                    TextBodyLarge(
                        text = stringResource(R.string.back),
                        color = MaterialTheme.colorScheme.secondary
                    )
                }
                if (uiState is NewNoteScreenUiState.EditNote)
                    Icon(
                        modifier = Modifier
                            .size(26.dp)
                            .clickable {
                                vieWModel.onEvent(NewNoteScreenEvents.DeleteNote)
                                onBackClick()
                            },
                        imageVector = AppOutlinedIcons.Delete,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.secondary
                    )
            }
            18.ToVerticalSpace()
            Column(
                modifier = Modifier
                    .padding(start = 20.dp)
                    .verticalScroll(rememberScrollState())
            ) {
                TextBodyMedium(
                    text = dateTimeFormat(dateTime.longValue),
                    color = MaterialTheme.colorScheme.secondary
                )
                28.ToVerticalSpace()
                TitleTextField(
                    modifier = Modifier.fillMaxWidth(),
                    placeHolder = stringResource(R.string.title),
                    value = noteTitleValue.value
                ) { newValue -> noteTitleValue.value = newValue }
                28.ToVerticalSpace()
                BodyTextField(
                    modifier = Modifier.fillMaxWidth().defaultMinSize(minHeight = 200.dp),
                    placeHolder = stringResource(R.string.write_something_about),
                    value = noteDescriptionValue.value
                ) { newValue -> noteDescriptionValue.value = newValue }
                28.ToVerticalSpace()
            }
        }
        NotePriority(
            selectedNoteColor.value,
            onNoteColorChanged = {
                selectedNoteColor.value = it
            }
        )
    }
}

@Composable
private fun BoxScope.NotePriority(
    selectedNoteColor: NoteColor,
    onNoteColorChanged: (NoteColor) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .align(Alignment.BottomStart)
            .padding(bottom = 30.dp, top = 8.dp)
            .background(color = MaterialTheme.colorScheme.background),
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Modifier
                .size(32.dp)
                .background(
                    color = DarkNoteGreenBackground.copy(if (selectedNoteColor == NoteColor.GREEN) 1f else SELECTED_NOTE_COLOR_ALPHA),
                    shape = CircleShape
                )
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = rememberRipple(bounded = false),
                    onClick = { onNoteColorChanged(NoteColor.GREEN) }
                ),
        )
        Box(
            modifier = Modifier
                .size(32.dp)
                .background(
                    color = DarkNoteYellowBackground.copy(if (selectedNoteColor == NoteColor.YELLOW) 1f else SELECTED_NOTE_COLOR_ALPHA),
                    shape = CircleShape
                )
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = rememberRipple(
                        bounded = false
                    ),
                    onClick = {
                        onNoteColorChanged(NoteColor.YELLOW)
                    }
                ),
        )
        Box(
            modifier = Modifier
                .size(32.dp)
                .background(
                    color = DarkNoteRedBackground.copy(if (selectedNoteColor == NoteColor.RED) 1f else SELECTED_NOTE_COLOR_ALPHA),
                    shape = CircleShape
                )
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = rememberRipple(
                        bounded = false
                    ),
                    onClick = {
                        onNoteColorChanged(NoteColor.RED)
                    }
                )
        )
    }
}