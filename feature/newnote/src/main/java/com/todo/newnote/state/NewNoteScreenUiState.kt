package com.todo.newnote.state

import com.todo.model.NoteResource

sealed interface NewNoteScreenUiState {

    data object NewNote : NewNoteScreenUiState

    data class EditNote(
        val note : NoteResource?
    ) : NewNoteScreenUiState

}