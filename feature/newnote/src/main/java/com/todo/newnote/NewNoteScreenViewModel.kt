package com.todo.newnote

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.todo.domain.notes.AddNewNoteUseCase
import com.todo.domain.notes.DeleteNoteUseCase
import com.todo.domain.notes.GetNoteByIdUseCase
import com.todo.domain.notes.UpdateNoteUseCase
import com.todo.model.NoteColor
import com.todo.newnote.event.NewNoteScreenEvents
import com.todo.newnote.navigation.NOTE_ARG
import com.todo.newnote.state.NewNoteScreenUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewNoteScreenViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    getNoteByIdUseCase: GetNoteByIdUseCase,
    private val addNewNoteUseCase: AddNewNoteUseCase,
    private val updateNoteUseCase: UpdateNoteUseCase,
    private val deleteNoteUseCase: DeleteNoteUseCase
) : ViewModel() {

    private val noteId: Int? = savedStateHandle[NOTE_ARG]

    private val previousNote = noteId?.let {
        getNoteByIdUseCase.invoke(noteId)
    }

    private val _newNoteScreenUiState =
        MutableStateFlow(
            if (noteId != null) NewNoteScreenUiState.EditNote(previousNote)
            else NewNoteScreenUiState.NewNote
        )
    val newNoteScreenUiState: StateFlow<NewNoteScreenUiState> get() = _newNoteScreenUiState

    fun onEvent(event: NewNoteScreenEvents) = viewModelScope.launch {
        when (event) {
            is NewNoteScreenEvents.AddOrUpdateNote -> addOrUpdateNote(
                noteTitle = event.title,
                noteDescription = event.description,
                noteColor = event.noteColor
            )

            is NewNoteScreenEvents.DeleteNote -> previousNote?.id?.let {
                    deleteNoteUseCase.invoke(it)
                }
        }
    }

    private suspend fun addOrUpdateNote(
        noteTitle: String,
        noteDescription: String,
        noteColor: NoteColor
    ) = when (_newNoteScreenUiState.value) {
        is NewNoteScreenUiState.NewNote -> addNewNote(noteTitle, noteDescription, noteColor)

        is NewNoteScreenUiState.EditNote -> modifyNote(noteTitle, noteDescription, noteColor)

    }

    private suspend fun addNewNote(
        noteTitle : String,
        noteDescription: String,
        noteColor: NoteColor
    ) {
        isNoteCanBeAdded(noteTitle, noteDescription).let {
            if (it) addNewNoteUseCase.invoke(
                noteTitle = noteTitle,
                noteDescription = noteDescription,
                noteColor = noteColor
            )
        }
    }

    private suspend fun modifyNote(
        noteTitle : String,
        noteDescription: String,
        noteColor: NoteColor
    ) {
        isNoteRequiredToUpdate(noteTitle, noteDescription, noteColor)?.let { requiredToUpdate ->
            previousNote?.id?.let { noteId ->
                if (noteTitle.isEmpty() && noteDescription.isEmpty()) {
                    deleteNoteUseCase.invoke(noteId)
                    @Suppress("LABEL_NAME_CLASH")
                    return@let
                }
                if (requiredToUpdate) updateNoteUseCase.invoke(
                    noteId,
                    noteTitle,
                    noteDescription,
                    noteColor
                )
            }
        }
    }

    private fun isNoteCanBeAdded(
        noteTitle: String,
        noteDescription: String
    ) = noteTitle.isNotEmpty() || noteDescription.isNotEmpty()

    private fun isNoteRequiredToUpdate(
        noteTitle: String,
        noteDescription: String,
        noteColor: NoteColor
    ) = previousNote?.let {
        noteTitle != previousNote.title || noteDescription != previousNote.description || noteColor != previousNote.color
    }
}