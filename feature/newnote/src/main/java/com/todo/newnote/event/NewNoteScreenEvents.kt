package com.todo.newnote.event

import com.todo.model.NoteColor

sealed interface NewNoteScreenEvents {

    data class AddOrUpdateNote(val title : String , val description : String , val noteColor: NoteColor) : NewNoteScreenEvents

    data object DeleteNote : NewNoteScreenEvents

}