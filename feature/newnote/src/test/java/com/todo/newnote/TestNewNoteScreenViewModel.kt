package com.todo.newnote

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.todo.domain.notes.AddNewNoteUseCase
import com.todo.domain.notes.DeleteNoteUseCase
import com.todo.domain.notes.GetNoteByIdUseCase
import com.todo.domain.notes.UpdateNoteUseCase
import com.todo.model.NoteColor
import com.todo.model.NoteResource
import com.todo.model.createNoteResource
import com.todo.newnote.event.NewNoteScreenEvents
import com.todo.newnote.navigation.NOTE_ARG
import com.todo.newnote.state.NewNoteScreenUiState
import com.todo.testing.utils.MainDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
@SmallTest
class TestNewNoteScreenViewModel {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    lateinit var savedStateHandle: SavedStateHandle

    @Mock
    lateinit var getNoteByIdUseCase: GetNoteByIdUseCase

    @Mock
    lateinit var addNewNoteUseCase: AddNewNoteUseCase

    @Mock
    lateinit var updateNoteUseCase: UpdateNoteUseCase

    @Mock
    lateinit var deleteNoteUseCase: DeleteNoteUseCase

    private lateinit var viewModel: NewNoteScreenViewModel

    @Before
    fun setup() {
        viewModel = createViewModel(savedStateHandle)
    }

    private fun createViewModel(savedStateHandle: SavedStateHandle): NewNoteScreenViewModel {
        return NewNoteScreenViewModel(
            savedStateHandle,
            getNoteByIdUseCase,
            addNewNoteUseCase,
            updateNoteUseCase,
            deleteNoteUseCase
        )
    }

    private fun createSavedStateHandleWithNoteIdArg(noteId: Int): SavedStateHandle {
        return SavedStateHandle().apply {
            set(NOTE_ARG, noteId)
        }
    }

    private fun createUpdatedNote(): NoteResource {
        return createNoteResource(
            noteTitle = "Edited Title",
            noteDescription = "Edited Description",
            noteColor = NoteColor.RED
        )
    }

    private fun createEmptyNote(): NoteResource {
        return createNoteResource(
            noteTitle = "",
            noteDescription = "",
            noteColor = NoteColor.RED
        )
    }

    private fun mockGetNoteById(note: NoteResource) {
        `when`(getNoteByIdUseCase.invoke(anyInt())).thenReturn(note)
    }

    private fun verifyUpdateNoteUseCase(updatedNote: NoteResource) {
        runTest {
            verify(updateNoteUseCase).invoke(
                noteId = updatedNote.id,
                noteTitle = updatedNote.title,
                noteDescription = updatedNote.description,
                noteColor = updatedNote.color
            )
        }
    }

    private fun verifyDeleteNoteUseCase(updatedNote: NoteResource) {
        runTest {
            verify(deleteNoteUseCase).invoke(
                noteId = updatedNote.id
            )
        }
    }

    @Test
    fun `test viewModel initialization with new note`() {
        val emptySavedStateHandle = SavedStateHandle()
        viewModel = createViewModel(emptySavedStateHandle)
        assertThat(viewModel.newNoteScreenUiState.value).isEqualTo(NewNoteScreenUiState.NewNote)
    }

    @Test
    fun `test viewModel initialization with existing note`() {
        val savedStateHandle = createSavedStateHandleWithNoteIdArg(1)
        viewModel = createViewModel(savedStateHandle)

        assertThat(viewModel.newNoteScreenUiState.value is NewNoteScreenUiState.EditNote).isTrue()
    }

    @Test
    fun `test add new note`() {
        val note = createNoteResource(
            noteTitle = "Title",
            noteDescription = "Description",
            noteColor = NoteColor.RED
        )
        viewModel.onEvent(
            NewNoteScreenEvents.AddOrUpdateNote(
                title = note.title,
                description = note.description,
                noteColor = note.color
            )
        )
        runTest {
            verify(addNewNoteUseCase).invoke(note.title, note.description, note.color)
        }
    }

    @Test
    fun `test edit note`() {
        val savedStateHandle = createSavedStateHandleWithNoteIdArg(1)
        val previousNote = createNoteResource(
            noteTitle = "Title",
            noteDescription = "Description",
            noteColor = NoteColor.RED
        )
        mockGetNoteById(previousNote)

        viewModel = createViewModel(savedStateHandle)

        val updatedNote = createUpdatedNote()
        updateNoteInViewModel(updatedNote)

        verifyUpdateNoteUseCase(updatedNote)
    }

    @Test
    fun `test delete note`() {
        val savedStateHandle = createSavedStateHandleWithNoteIdArg(1)
        val previousNote = createNoteResource(
            noteTitle = "Title",
            noteDescription = "Description",
            noteColor = NoteColor.RED
        )
        mockGetNoteById(previousNote)

        viewModel = createViewModel(savedStateHandle)

        val emptyNote = createEmptyNote()
        updateNoteInViewModel(emptyNote)

        verifyDeleteNoteUseCase(emptyNote)
    }

    private fun updateNoteInViewModel(updatedNote: NoteResource) {
        viewModel.onEvent(
            NewNoteScreenEvents.AddOrUpdateNote(
                title = updatedNote.title,
                description = updatedNote.description,
                noteColor = updatedNote.color
            )
        )
    }

}