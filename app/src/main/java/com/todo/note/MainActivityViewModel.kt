package com.todo.note

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.todo.domain.app_settings.GetAppSettingsUseCase
import com.todo.model.AppSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val getAppSettingsUseCase: GetAppSettingsUseCase
) : ViewModel() {

    private var _appSettings = MutableStateFlow(AppSettings())
    val appSettings : StateFlow<AppSettings> get() = _appSettings

    init {
        viewModelScope.launch {
            getAppSettingsUseCase.invoke().collect {
                _appSettings.value = it
            }
        }
    }

}