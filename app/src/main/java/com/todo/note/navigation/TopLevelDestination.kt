package com.todo.note.navigation

import com.todo.newnote.navigation.newNoteScreenRoute
import com.todo.notes.navigation.notesScreenRoute
import com.todo.settings.navigation.settingsScreenRoute

enum class TopLevelDestination(val route : String) {
    NOTES(notesScreenRoute),
    NEW_NOTE(newNoteScreenRoute),
    SETTINGS(settingsScreenRoute)
}