package com.todo.note.navigation

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.todo.newnote.navigation.navigateToEditNoteScreen
import com.todo.newnote.navigation.navigateToNewNoteScreen
import com.todo.newnote.navigation.newNoteScreen
import com.todo.notes.navigation.notesScreen
import com.todo.settings.navigation.navigateToSettingsScreen
import com.todo.settings.navigation.settingsScreen

@Composable
fun NoteNaveHost(
    modifier: Modifier = Modifier,
    startDestination: TopLevelDestination
) {
    val navHostController = rememberNavController()
    NavHost(
        modifier = modifier.fillMaxSize(),
        navController = navHostController,
        startDestination = startDestination.route
    ) {
        notesScreen(
            onAddNewNoteClick = {
                navHostController.navigateToNewNoteScreen(null)
            },
            onSettingButtonClick = {
                navHostController.navigateToSettingsScreen()
            },
            onNoteClick = {
                navHostController.navigateToEditNoteScreen(it.id, null)
            })
        newNoteScreen(
            onBackClick = { navHostController.popBackStack() }
        )
        settingsScreen()
    }
}