package com.todo.database_test

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.todo.database.NoteDatabase
import com.todo.database.dao.NoteDao
import com.todo.database_test.utils.createNoteEntity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class NoteDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var noteDatabase: NoteDatabase
    private lateinit var noteDao: NoteDao

    @Before
    fun setup() {
        noteDatabase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(), NoteDatabase::class.java
        ).build()
        noteDao = noteDatabase.getNoteDao()
    }

    @After
    fun teardown() {
        noteDatabase.close()
    }

    @Test
    fun insert() = runBlocking {
        val note = createNoteEntity()
        noteDao.insert(note)
        val notes = noteDao.getAllNotes().first()
        assertThat(notes.find {
            it.title == note.title &&
                    it.color == note.color &&
                    it.description == note.description &&
                    it.creationDate == note.creationDate
        }).isNotNull()
    }

    @Test
    fun update() = runBlocking {
        val note = createNoteEntity()
        noteDao.insert(note)
        val notesBeforeUpdate = noteDao.getAllNotes().first()
        assertThat(notesBeforeUpdate.find { it.title == note.title }).isNotNull()

        val updatedNote = notesBeforeUpdate.first().copy(title = "Updated title")
        noteDao.update(updatedNote)
        val notesAfterUpdate = noteDao.getAllNotes().first()
        assertThat(notesAfterUpdate.find { it.title == updatedNote.title }).isNotNull()
    }

    @Test
    fun delete() = runBlocking {
        val note = createNoteEntity()
        noteDao.insert(note)

        val notesBeforeDelete = noteDao.getAllNotes().first()
        noteDao.delete(notesBeforeDelete.first().id)

        val notesAfterDelete = noteDao.getAllNotes().first()
        assertThat(notesAfterDelete.find {
            it.title == note.title &&
                    it.color == note.color &&
                    it.description == note.description &&
                    it.creationDate == note.creationDate
        }).isNull()
    }

    @Test
    fun search() = runBlocking {
        val note = createNoteEntity()
        noteDao.insert(note)

        val relatedSearchResult = noteDao.search(note.title).first()
        assertThat(relatedSearchResult).isNotEmpty()

        val notRelatedSearchResult = noteDao.search(note.description).first()
        assertThat(notRelatedSearchResult).isEmpty()
    }

    @Test
    fun getAllNotes() = runBlocking {
        val note = createNoteEntity()
        noteDao.insert(note)

        assertThat(noteDao.getAllNotes().first()).isNotEmpty()
    }

    @Test
    fun getNoteById() = runBlocking {
        val note = createNoteEntity()
        noteDao.insert(note)

        val noteId = noteDao.getAllNotes().first().first().id
        assertThat(noteDao.getNoteById(noteId)).isNotNull()
    }
}