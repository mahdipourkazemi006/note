package com.todo.database_test.utils

import com.todo.database.model.NoteEntity
import com.todo.model.NoteColor
import com.todo.model.NoteResource

fun createNoteEntity() = NoteEntity(
    id = 1,
    title = "Title test",
    description = "Description test",
    creationDate = System.currentTimeMillis(),
    color = NoteColor.GREEN
)