package com.todo.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.todo.designsystem.component.TextBodyMedium

@Composable
fun NoteCategoryItem(
    modifier: Modifier = Modifier,
    categoryName: String,
    isSelectedCategory: Boolean,
    onCategoryClick: (String) -> Unit
) {
    Box(
        modifier
            .background(
                if (isSelectedCategory) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.primaryContainer,
                shape = Shapes().small
            )
            .padding(13.dp)
            .clickable { onCategoryClick(categoryName) },
        contentAlignment = Alignment.Center
    ) {
        TextBodyMedium(
            text = categoryName,
            color = if (isSelectedCategory) MaterialTheme.colorScheme.onPrimary else MaterialTheme.colorScheme.primary
        )
    }
}