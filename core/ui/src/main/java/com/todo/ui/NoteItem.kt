package com.todo.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.todo.designsystem.component.TextBodyMedium
import com.todo.designsystem.component.TextHeaderMedium
import com.todo.designsystem.component.TextLabelSmall
import com.todo.designsystem.theme.DarkNoteGreenBackground
import com.todo.designsystem.theme.DarkNoteRedBackground
import com.todo.designsystem.theme.DarkNoteYellowBackground
import com.todo.designsystem.theme.LightNoteGreenBackground
import com.todo.designsystem.theme.LightNoteRedBackground
import com.todo.designsystem.theme.LightNoteYellowBackground
import com.todo.model.AppTheme
import com.todo.model.NoteColor
import com.todo.model.NoteResource

private const val NOTE_DESCRIPTION_MAX_LINE = 16

@Composable
fun NoteItem(
    modifier: Modifier = Modifier,
    appTheme: AppTheme,
    note: NoteResource,
    onNoteClick: (NoteResource) -> Unit
) {
    val noteColor = when {
        note.color == NoteColor.GREEN && if (appTheme == AppTheme.AUTOMATIC) !isSystemInDarkTheme() else appTheme == AppTheme.LIGHT -> LightNoteGreenBackground
        note.color == NoteColor.GREEN && if (appTheme == AppTheme.AUTOMATIC) isSystemInDarkTheme() else appTheme == AppTheme.DARK -> DarkNoteGreenBackground
        note.color == NoteColor.YELLOW && if (appTheme == AppTheme.AUTOMATIC) !isSystemInDarkTheme() else appTheme == AppTheme.LIGHT -> LightNoteYellowBackground
        note.color == NoteColor.YELLOW && if (appTheme == AppTheme.AUTOMATIC) isSystemInDarkTheme() else appTheme == AppTheme.DARK -> DarkNoteYellowBackground
        note.color == NoteColor.RED && if (appTheme == AppTheme.AUTOMATIC) !isSystemInDarkTheme() else appTheme == AppTheme.LIGHT -> LightNoteRedBackground
        note.color == NoteColor.RED && if (appTheme == AppTheme.AUTOMATIC) isSystemInDarkTheme() else appTheme == AppTheme.DARK -> DarkNoteRedBackground
        else -> LightNoteGreenBackground
    }
    Box(
        modifier = modifier
    ) {
        Column(modifier = modifier
            .background(
                color = noteColor, shape = Shapes().medium
            )
            .clickable { onNoteClick(note) }
            .padding(12.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.Start) {
            if (note.title.isNotEmpty()) {
                TextHeaderMedium(
                    maxLine = 2,
                    text = note.title,
                    color = Color.Black,
                    ellipsis = TextOverflow.Ellipsis
                )
                4.ToVerticalSpace()
            }
            TextLabelSmall(
                text = dateTimeFormat(note.creationDate), color = Color.Black.copy(0.6f)
            )
            10.ToVerticalSpace()
            TextBodyMedium(
                text = note.description,
                color = Color.Black,
                maxLine = NOTE_DESCRIPTION_MAX_LINE,
                ellipsis = TextOverflow.Ellipsis
            )
        }
        if (note.description.length > 300) Box(
            modifier = modifier
                .height(78.dp)
                .background(
                    brush = Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            noteColor.copy(alpha = 0.3f),
                            noteColor.copy(alpha = 0.6f),
                            noteColor
                        )
                    ), shape = Shapes().medium
                )
                .align(Alignment.BottomStart)
        )
    }
}