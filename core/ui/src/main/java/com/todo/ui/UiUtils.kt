package com.todo.ui

import android.content.Context
import android.content.ContextWrapper
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

fun dateTimeFormat(millis: Long): String {
    val dateToFormat = Calendar.getInstance().apply {
        timeInMillis = millis
    }

    val today = Calendar.getInstance()
    val dateFormat: SimpleDateFormat

    // Check if the date to format is today
    return if (today.get(Calendar.YEAR) == dateToFormat.get(Calendar.YEAR) &&
        today.get(Calendar.DAY_OF_YEAR) == dateToFormat.get(Calendar.DAY_OF_YEAR)) {
        dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        "Today, ${dateFormat.format(dateToFormat.time)}"
    } else {
        dateFormat = SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.getDefault())
        dateFormat.format(dateToFormat.time)
    }
}