package com.todo.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.todo.designsystem.component.BodyTextField
import com.todo.designsystem.icon.AppRoundedIcons

@Composable
fun SearchBox(
    modifier: Modifier,
    placeholder: String,
    value : String,
    onSearch : (String) -> Unit
) {
    Row(
        modifier = modifier
            .background(
                color = MaterialTheme.colorScheme.outline.copy(0.1f),
                shape = Shapes().medium
            )
            .padding(12.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        BodyTextField(
            modifier = Modifier.fillMaxWidth(0.8f),
            singleLine = true,
            placeHolder = placeholder,
            value = value,
            onValueChange = onSearch
        )
        Icon(
            modifier = Modifier.size(20.dp),
            imageVector = AppRoundedIcons.Search,
            contentDescription = null,
            tint = MaterialTheme.colorScheme.outline
        )
    }
}