package com.todo.data

import androidx.datastore.core.DataStore
import com.todo.model.AppSettings
import com.todo.model.AppTheme
import com.todo.model.SortViewButtonStates
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AppSettingsRepositoryImpl @Inject constructor(
    private val appSettings : DataStore<AppSettings>
) : AppSettingsRepository {

    override suspend fun setAppTheme(theme: AppTheme) {
        appSettings.updateData {
            it.copy(
                theme = theme
            )
        }
    }

    override suspend fun setNoteSortType(sortType: SortViewButtonStates) {
        appSettings.updateData {
            it.copy(
                noteSort = sortType
            )
        }
    }

    override fun getAppSettings(): Flow<AppSettings> = appSettings.data

}