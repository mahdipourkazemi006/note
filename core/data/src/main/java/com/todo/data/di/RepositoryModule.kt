package com.todo.data.di

import com.todo.data.AppSettingsRepository
import com.todo.data.AppSettingsRepositoryImpl
import com.todo.data.NotesRepository
import com.todo.data.NotesRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun bindNotesRepositoryToNotesRepositoryImpl(impl: NotesRepositoryImpl): NotesRepository

    @Binds
    fun bindAppSettingsRepositoryToAppSettingsRepositoryImpl(impl: AppSettingsRepositoryImpl): AppSettingsRepository

}