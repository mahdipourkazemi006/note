package com.todo.data

import com.todo.model.AppSettings
import com.todo.model.AppTheme
import com.todo.model.SortViewButtonStates
import kotlinx.coroutines.flow.Flow

interface AppSettingsRepository {

    suspend fun setAppTheme(theme : AppTheme)

    suspend fun setNoteSortType(sortType : SortViewButtonStates)

    fun getAppSettings() : Flow<AppSettings>

}