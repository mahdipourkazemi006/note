package com.todo.data

import com.todo.database.model.NoteEntity
import com.todo.model.NoteResource
import kotlinx.coroutines.flow.Flow

interface NotesRepository {

    suspend fun addNote(note : NoteResource)

    suspend fun updateNote(note: NoteResource)

    suspend fun deleteNote(noteId: Int)

    fun search(query: String) : Flow<List<NoteResource>>

    fun getNoteById(noteId : Int) : NoteResource?

    fun getAllNote() : Flow<List<NoteResource>>

}