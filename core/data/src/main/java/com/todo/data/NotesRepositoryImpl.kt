package com.todo.data

import com.todo.database.dao.NoteDao
import com.todo.database.model.toEntity
import com.todo.database.model.toUiModel
import com.todo.model.NoteResource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class NotesRepositoryImpl @Inject constructor(
    private val noteDao: NoteDao
) : NotesRepository {

    override suspend fun addNote(note: NoteResource) = noteDao.insert(note.toEntity())

    override suspend fun updateNote(note: NoteResource) =
        noteDao.update(note.toEntity())

    override suspend fun deleteNote(noteId: Int) = noteDao.delete(noteId)

    override fun search(query: String) = noteDao.search(query).map {
            listOfNotes -> listOfNotes.map { noteEntity -> noteEntity.toUiModel() }
    }

    override fun getNoteById(noteId: Int): NoteResource = noteDao.getNoteById(noteId)

    override fun getAllNote(): Flow<List<NoteResource>> =
        noteDao.getAllNotes()
            .map { listOfNotes -> listOfNotes.map { noteEntity -> noteEntity.toUiModel() } }

}