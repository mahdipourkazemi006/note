package com.todo.database.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.todo.model.NoteColor

class NoteColorConverter {

    @TypeConverter
    fun noteColorToString(noteColor: NoteColor) = Gson().toJson(noteColor.name)

    @TypeConverter
    fun stringToNoteColor(string : String) = Gson().fromJson(string , NoteColor::class.java)

}