package com.todo.database.di

import com.todo.database.NoteDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DaosModule {

    @Provides
    @Singleton
    fun provideNoteDao(
        noteDatabase: NoteDatabase
    ) = noteDatabase.getNoteDao()

}