package com.todo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.todo.database.dao.NoteDao
import com.todo.database.model.NoteEntity
import com.todo.database.utils.NoteColorConverter

@Database(
    entities = [NoteEntity::class],
    version = 1
)
@TypeConverters(
    NoteColorConverter::class
)
abstract class NoteDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "note_database"
    }

    abstract fun getNoteDao() : NoteDao

}