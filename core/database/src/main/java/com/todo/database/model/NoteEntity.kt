package com.todo.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.todo.database.model.NoteEntity.Companion.TABLE_NAME
import com.todo.model.NoteColor
import com.todo.model.NoteResource

@Entity(tableName = TABLE_NAME)
data class NoteEntity(
    @PrimaryKey(autoGenerate = true)
    val id : Int = 0,
    val title : String,
    val description : String,
    val creationDate : Long,
    val color : NoteColor
) {
    companion object{
        const val TABLE_NAME = "note_table"
    }
}

fun NoteResource.toEntity() = NoteEntity(
    id = this.id,
    title = this.title,
    description = this.description,
    creationDate = this.creationDate,
    color = this.color
)

fun NoteEntity.toUiModel() = NoteResource(
    id = this.id,
    title = this.title,
    description = this.description,
    creationDate = this.creationDate,
    color = this.color
)