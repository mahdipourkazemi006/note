package com.todo.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.todo.database.model.NoteEntity
import com.todo.model.NoteColor
import com.todo.model.NoteResource
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(note : NoteEntity)

    @Update
    suspend fun update(note : NoteEntity)

    @Query("DELETE FROM ${NoteEntity.TABLE_NAME} WHERE id = :noteId")
    suspend fun delete(noteId: Int)

    @Query("SELECT * FROM ${NoteEntity.TABLE_NAME} WHERE title LIKE '%' || :query || '%' ORDER BY creationDate DESC")
    fun search(query : String) : Flow<List<NoteEntity>>

    @Query("SELECT * FROM ${NoteEntity.TABLE_NAME} ORDER BY creationDate DESC")
    fun getAllNotes() : Flow<List<NoteEntity>>

    @Query("SELECT * FROM ${NoteEntity.TABLE_NAME} WHERE id = :noteId")
    fun getNoteById(noteId : Int) : NoteResource

}