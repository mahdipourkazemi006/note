package com.todo.designsystem.icon

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.outlined.GridView
import androidx.compose.material.icons.outlined.Settings

object AppOutlinedIcons {
    val GridView = Icons.Outlined.GridView
    val Settings = Icons.Outlined.Settings
    val Delete = Icons.Outlined.Delete
}