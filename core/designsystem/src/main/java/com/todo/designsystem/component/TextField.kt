package com.todo.designsystem.component

import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.todo.designsystem.theme.Typography

@Composable
fun TitleTextField(
    modifier: Modifier = Modifier,
    placeHolder: String,
    value: String,
    onValueChange: (String) -> Unit
) {
    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        textStyle = Typography.headlineMedium.copy(color = MaterialTheme.colorScheme.onBackground),
        decorationBox = {
            value.ifEmpty {
                TextHeaderMedium(text = placeHolder, color = MaterialTheme.colorScheme.outlineVariant)
            }
            it()
        }
    )
}

@Composable
fun BodyTextField(
    modifier: Modifier = Modifier,
    placeHolder: String,
    value: String,
    maxLine : Int = Int.MAX_VALUE,
    singleLine : Boolean = false,
    onValueChange: (String) -> Unit
) {
    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        textStyle = Typography.bodyLarge.copy(color = MaterialTheme.colorScheme.onSurfaceVariant),
        maxLines = maxLine,
        singleLine = singleLine,
        decorationBox = {
            value.ifEmpty {
                TextBodyLarge(text = placeHolder, color = MaterialTheme.colorScheme.outline)
            }
            it()
        }
    )
}