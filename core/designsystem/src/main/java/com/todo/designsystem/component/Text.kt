package com.todo.designsystem.component

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import com.todo.designsystem.theme.Typography

@Composable
fun TextHeaderLarge(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.headlineLarge.copy(color = color)
    )
}

@Composable
fun TextHeaderMedium(
    modifier: Modifier = Modifier,
    text : String,
    maxLine : Int = Int.MAX_VALUE,
    color: Color,
    ellipsis : TextOverflow = TextOverflow.Clip
) {
    Text(
        text = text,
        modifier = modifier,
        maxLines = maxLine,
        style = Typography.headlineMedium.copy(color = color),
        overflow = ellipsis
    )
}

@Composable
fun TextHeaderSmall(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.headlineSmall.copy(color = color)
    )
}

@Composable
fun TextTitleLarge(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.titleLarge.copy(color = color)
    )
}

@Composable
fun TextTitleMedium(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.titleMedium.copy(color = color)
    )
}

@Composable
fun TextTitleSmall(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.titleSmall.copy(color = color)
    )
}

@Composable
fun TextBodyLarge(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.bodyLarge.copy(color = color)
    )
}

@Composable
fun TextBodyMedium(
    modifier: Modifier = Modifier,
    text : String,
    maxLine : Int = Int.MAX_VALUE,
    textAlign : TextAlign = TextAlign.Start,
    color: Color,
    ellipsis: TextOverflow = TextOverflow.Clip
) {
    Text(
        text = text,
        modifier = modifier,
        maxLines = maxLine,
        style = Typography.bodyMedium.copy(color = color , textAlign = textAlign),
        overflow = ellipsis
    )
}

@Composable
fun TextBodySmall(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.bodySmall.copy(color = color)
    )
}

@Composable
fun TextLabelLarge(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.labelLarge.copy(color = color)
    )
}

@Composable
fun TextLabelMedium(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.labelMedium.copy(color = color)
    )
}

@Composable
fun TextLabelSmall(
    modifier: Modifier = Modifier,
    text : String,
    color: Color
) {
    Text(
        text = text,
        modifier = modifier,
        style = Typography.labelSmall.copy(color = color)
    )
}