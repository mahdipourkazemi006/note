package com.todo.designsystem.icon

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.List
import androidx.compose.material.icons.outlined.GridView
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.ArrowBackIosNew
import androidx.compose.material.icons.rounded.Search
import androidx.compose.ui.graphics.vector.ImageVector

object AppRoundedIcons {
    val Add = Icons.Rounded.Add
    val Search = Icons.Rounded.Search
    val Back = Icons.Rounded.ArrowBackIosNew
    val ListView = Icons.AutoMirrored.Outlined.List
}