package com.todo.designsystem.component

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.todo.designsystem.icon.AppOutlinedIcons
import com.todo.designsystem.icon.AppRoundedIcons
import com.todo.model.SortViewButtonStates

@Composable
fun SortViewButton(
    modifier: Modifier = Modifier,
    buttonState: SortViewButtonStates,
    onClick: (SortViewButtonStates) -> Unit,
) {
    Box(
        modifier = modifier
            .background(
                color = MaterialTheme.colorScheme.primaryContainer,
                shape = Shapes().small
            )
            .clickable {
                onClick(
                    if (buttonState == SortViewButtonStates.GRID) SortViewButtonStates.LIST
                    else SortViewButtonStates.GRID
                )
            },
        contentAlignment = Alignment.Center
    ) {
        Icon(
            modifier = Modifier.size(26.dp),
            imageVector = when (buttonState) {
                SortViewButtonStates.GRID -> AppRoundedIcons.ListView
                SortViewButtonStates.LIST -> AppOutlinedIcons.GridView
            },
            contentDescription = null,
            tint = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.5f)
        )
    }
}