package com.todo.domain.app_settings

import com.todo.data.AppSettingsRepository
import com.todo.model.SortViewButtonStates
import javax.inject.Inject

class UpdateNoteSortTypeUseCase @Inject constructor(
    private val appSettingsRepository: AppSettingsRepository
) {

    suspend operator fun invoke(noteSort: SortViewButtonStates) =
        appSettingsRepository.setNoteSortType(noteSort)

}