package com.todo.domain.notes

import com.todo.data.NotesRepository
import com.todo.model.NoteResource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SearchNoteUseCase @Inject constructor(
    private val notesRepository: NotesRepository
) {

    operator fun invoke(query : String): Flow<List<NoteResource>> = notesRepository.search(query)

}