package com.todo.domain.app_settings

import com.todo.data.AppSettingsRepository
import com.todo.model.AppTheme
import javax.inject.Inject

class UpdateAppThemeUseCase @Inject constructor(
    private val appSettingsRepository: AppSettingsRepository
) {

    suspend operator fun invoke(theme : AppTheme) = appSettingsRepository.setAppTheme(theme)

}