package com.todo.domain.notes

import com.todo.data.NotesRepository
import com.todo.model.NoteColor
import com.todo.model.createNoteResource
import javax.inject.Inject

class AddNewNoteUseCase @Inject constructor(
    private val notesRepository: NotesRepository
) {

    suspend operator fun invoke(noteTitle: String, noteDescription: String, noteColor: NoteColor) =
        notesRepository.addNote(
            createNoteResource(
                noteTitle = noteTitle,
                noteDescription = noteDescription,
                noteColor = noteColor
            )
        )

}