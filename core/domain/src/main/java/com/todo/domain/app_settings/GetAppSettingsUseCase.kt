package com.todo.domain.app_settings

import com.todo.data.AppSettingsRepository
import javax.inject.Inject

class GetAppSettingsUseCase @Inject constructor(
    private val appSettingsRepository: AppSettingsRepository
) {

    operator fun invoke() = appSettingsRepository.getAppSettings()

}