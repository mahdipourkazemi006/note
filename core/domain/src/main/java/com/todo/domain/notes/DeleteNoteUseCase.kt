package com.todo.domain.notes

import com.todo.data.NotesRepository
import com.todo.model.NoteResource
import javax.inject.Inject

class DeleteNoteUseCase @Inject constructor(
    private val notesRepository: NotesRepository
) {

    suspend operator fun invoke(noteId: Int) = notesRepository.deleteNote(noteId)

}