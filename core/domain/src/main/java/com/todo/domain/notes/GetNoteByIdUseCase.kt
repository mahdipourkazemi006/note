package com.todo.domain.notes

import com.todo.data.NotesRepository
import com.todo.model.NoteResource
import javax.inject.Inject

class GetNoteByIdUseCase @Inject constructor(
    private val notesRepository: NotesRepository
) {

    operator fun invoke(noteId : Int): NoteResource? = notesRepository.getNoteById(noteId)

}