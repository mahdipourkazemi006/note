package com.todo.domain.notes

import com.todo.data.NotesRepository
import com.todo.model.NoteResource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllNoteUseCase @Inject constructor(
    private val notesRepository: NotesRepository
) {

    operator fun invoke(): Flow<List<NoteResource>> = notesRepository.getAllNote()

}