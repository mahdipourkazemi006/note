package com.todo.domain.notes

import com.todo.data.NotesRepository
import com.todo.model.NoteColor
import com.todo.model.createNoteResource
import javax.inject.Inject

class UpdateNoteUseCase @Inject constructor(
    private val notesRepository: NotesRepository
) {

    suspend operator fun invoke(
        noteId: Int,
        noteTitle: String,
        noteDescription: String,
        noteColor: NoteColor
    ) =
        notesRepository.updateNote(
            createNoteResource(
                noteId,
                noteTitle,
                noteDescription,
                noteColor
            )
        )

}