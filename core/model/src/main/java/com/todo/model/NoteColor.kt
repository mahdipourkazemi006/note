package com.todo.model

enum class NoteColor {
    GREEN,
    YELLOW,
    RED
}