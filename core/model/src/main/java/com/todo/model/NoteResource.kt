package com.todo.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NoteResource(
    val id: Int,
    var title: String,
    var description: String,
    val creationDate: Long,
    val color: NoteColor
) : Parcelable

fun createNoteResource(
    noteId: Int = -1,
    noteTitle: String,
    noteDescription: String,
    noteColor: NoteColor
) = NoteResource(
    title = noteTitle,
    description = noteDescription,
    creationDate = System.currentTimeMillis(),
    color = noteColor,
    id = noteId
)