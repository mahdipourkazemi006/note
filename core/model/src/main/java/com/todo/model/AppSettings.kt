package com.todo.model

import kotlinx.serialization.Serializable

@Serializable
data class AppSettings(
    val theme : AppTheme = AppTheme.AUTOMATIC,
    val noteSort : SortViewButtonStates = SortViewButtonStates.LIST
) {
    companion object {
        const val APP_SETTINGS_DATA_STORE = "app_settings.json"
    }
}

enum class AppTheme {
    LIGHT,
    DARK,
    AUTOMATIC
}

enum class SortViewButtonStates {
    GRID,
    LIST
}