package com.todo.testing

import com.todo.data.NotesRepository
import com.todo.model.NoteResource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class TestNotesRepositoryImpl : NotesRepository {

    private val notes = mutableListOf<NoteResource>()

    override suspend fun addNote(note: NoteResource) {
        notes.add(note)
    }

    override suspend fun updateNote(note: NoteResource) {
        notes.find { it.id == note.id }?.let { targetNote ->
            val targetNoteIndex = notes.indexOf(targetNote)
            notes[targetNoteIndex] = targetNote.copy(
                id = note.id,
                title = note.title,
                description = note.description,
                creationDate = note.creationDate,
                color = note.color
            )
        }
    }

    override suspend fun deleteNote(noteId: Int) {
        notes.remove(notes.find { it.id == noteId })
    }

    override fun search(query: String): Flow<List<NoteResource>> = flow {
        notes.find { it.title == query }?.let { findNote ->
            emit(listOf(findNote))
        }
    }

    override fun getNoteById(noteId: Int): NoteResource? = notes.find { it.id == noteId }

    override fun getAllNote(): Flow<List<NoteResource>> = flow { emit(notes) }

}